#include <iostream>
#ifndef UNICODE
#define UNICODE
#endif
#ifndef _UNICODE
#define _UNICODE
#endif
#include <Windows.h>

using namespace std;

wstring tetromino[7];
int nFieldWidth = 12;
int nFieldHeight = 18;
unsigned char *pField = nullptr;

int nScreenWidth = 80; //console screen size X; standard
int nScreenHeight = 30; //console screen size Y; standard

/* Instead of having to draw out every possible block roatation,
    there's some two dimentional equations that we can use. */

int Rotate(int px, int py, int r)
{
    switch (r % 4)
    {
        case 0: //rotate 0 degrees
            return py * 4 + px; 
        case 1: //rotate 90 degrees
            return 12 + py - (px * 4);
        case 2: //rotate 180 degrees
            return 15 - (py * 4) - px;
        case 3: //rotate 270 degrees
            return 3 - py + (px * 4);
    }
}

int main()
{
    /* Assemble assets: In tetris, these are called "tetrominos" */
    
    //"I" 
    tetromino[7].append(L"..X.");
    tetromino[7].append(L"..X.");
    tetromino[7].append(L"..X.");
    tetromino[7].append(L"..X.");

    //"J"
    tetromino[7].append(L"....");
    tetromino[7].append(L"..XX");
    tetromino[7].append(L"..X.");
    tetromino[7].append(L"..X.");

    //"L"
    tetromino[7].append(L"....");
    tetromino[7].append(L"XX..");
    tetromino[7].append(L".X..");
    tetromino[7].append(L".X..");

    //"O"
    tetromino[7].append(L"....");
    tetromino[7].append(L"....");
    tetromino[7].append(L".XX.");
    tetromino[7].append(L".XX.");

    //"S"
    tetromino[7].append(L"....");
    tetromino[7].append(L"....");
    tetromino[7].append(L"..XX");
    tetromino[7].append(L"XX..");

    //"T"
    tetromino[7].append(L"....");
    tetromino[7].append(L"....");
    tetromino[7].append(L"..X.");
    tetromino[7].append(L".XXX");

    //"Z"
    tetromino[7].append(L"....");
    tetromino[7].append(L"....");
    tetromino[7].append(L"XX..");
    tetromino[7].append(L"..XX");

    /* Initializ array for playing field */
    pField = new unsigned char[nFieldWidth*nFieldHeight];
    for(int counter_x = 0; counter_x < nFieldWidth; counter_x++)
    {
        for (int counter_y = 0; counter_y < nFieldHeight; counter_y++)
        {
            pField[counter_y*nFieldWidth + counter_x] = 
                (counter_x == 0 || counter_x == nFieldWidth - 1 || 
                    counter_y == nFieldHeight -1) ? 9 : 0; 
        }
    }

    /* Code that allows us to use the Windows command line as a screen buffer... essentially */
    wchar_t *screen = new wchar_t[nScreenWidth*nScreenHeight];
    for(int counter = 0; counter < nScreenWidth*nScreenHeight; counter++)
    {
        screen[counter] = L' ';
    }
    HANDLE hConsole = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
    SetConsoleActiveScreenBuffer(hConsole);
    DWORD dwBytesWritten = 0;

    bool bGameOver = false;

    while(!bGameOver)
    {
        //Draw in playing field...
        for(int counter_x = 0; counter_x < nFieldWidth; counter_x++)
        {
            for(int counter_y = 0; counter_y < nFieldHeight; counter_y++)
            {
                screen[(counter_y + 2)*nScreenWidth + (counter_x + 2)] = L" ABCDEFG=#"[pField[(counter_y*nFieldWidth) + counter_x]];
            }
        }

        //This draws to the created screen buffer
        WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, {0, 0}, &dwBytesWritten);
    }
    return 0;
}